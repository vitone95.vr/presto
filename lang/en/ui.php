<?php
return [
    "allAnnouncements" => "Our announcements",
    "home" => "home",
    "annunci" => "announcements",
    "homeTitle" => "Presto.en",
    "registrati" => "register",
    "accedi" => "login",
    "zonaRevisori" => "Reviser area",
    "inserisciAnnuncio" => "Insert announcement",
    "benvenuto" => "Welcome",
    "logout" => "logout",
    "vuoiCreareAnnuncio" => "Would you like to create your announcement?",
    "crealo" => "Create",
    "annunciCliccati" => "The most clicked announcement",
    "vuoiVedereAnnunci" => "Would you like to see all the announcements?",
    "vaiAnnunci" => "Go to the announcements",
    "lavoraConNoi" => "Work whit us!",
    "diventaRevisore" => "Become reviser",
    "slogan" => "Welcome in the fantastic world of e-commerce with Presto! ©",
    "linkUtili" => "Useful links",
    "faq" => "FAQ",
    "about" => "About us",
    "cerca" => "Search",
    "cercaAnnuncio" => "search announcements",
    "vaiAiDettagli" => "view details",
    "filtraCategoria" => "Filter by category",
    "searchResult" => "Here are the results of your research:",
    "searchFailed" => "There are no announcements for your research, try with something else",
    "categoryNoAnnouncements" => "There are no announcements for this category!",
    "publicAnnouncement" => "Post it:",  
    "nuovoAnnouncio" => "new announcement",
    "accetta" => "accept",
    "rifiuta" => "refuse",
    "annulla" => "cancel",
    "annToRevise" => "Here is the announcement to review",
    "noAnnToRevise" => "There are no announcements to review",
    "titolo" => "title",
    "categoria" => "category",
    "prezzo" => "price",
    "descrizione" => "description",
    "pubblicato" => "published on",
    "autore" => "autor",
    "autoreAssente" => "no autor",
    "creaAnnuncio" => "Create your announcement",
    "selezionaCategoria" => "select category",
    "inserisciImmagine" => "insert images",
    "userMessage" => "An user want to work with us, here are his data:",
    "makeRevisor" => "If you want to make him revisor",
    "click" => "Click here",
    "nome" => "Name",
    "email" => "Email",
    "success" => "Congratulation, you accepted the announcement, do you want to undo the operation?",
    "failure" => "Congratulation, you refused the announcement, do you want to undo the operation?",
    "undo" => "Congratulation, you have undo the operation",
    "become" => "Congratulation! You asked to became revisor correctly",
    "make" => "The user became revisor, good work!",
    "denied" => "Warning! Only revisors can enter this page",
    "nomeUtente" => "User name",
    "password" => "Password",
    "passwordConfirm" => "Confirm Password",
    "accedi" => "Login",
    "registrati" => "Register",
    "giaRegistrato" => "Already registered?",
    "insertDati" => "Insert your data",
    "noRegistrato" => "Not registered?",
    "passDimenticata" => "Forgot Password?",
    "tuoiPreferiti" => "Your favorites",
    "modificaAnn" => "Edit",
    "eliminaAnn" => "Delete",
    "ciao" => "Hello",
    "telefono" => "Telephone",
    "indirizzo" => "Address",
    "età" => "Age",
    "preferiti" => "Your followed announcements",
    "tuoiAnn" => "Your announcements created",
    "irreversibile"=>"Are you sure about it? You can't undo this action",
    "eliminaProf"=>"Delete profile",
    "createProf"=>"Create your personal profile!",
    "foto"=>"Photo",
    "profilo"=>"User profile",
    "favoriti"=>"Your favourites",
];