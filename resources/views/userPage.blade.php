<x-layout>

    @if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
    @endif
    @if (session('delete'))
    <div class="alert alert-danger">
        {{ session('delete') }}
    </div>
    @endif

    @if (Auth::user()->userprofile)
    <h1 class="text-center my-5">{{__('ui.ciao')}} {{Auth::user()->name}} </h1>
    <div class="container">
        <div class="row justify-content-center">
                    <div class="card my-2" style="width: 18rem;">
                        <div class="card-body text-center">
                            <img src="{{Storage::url(Auth::user()->userprofile->photo)}}" class="card-img-top " alt="profile photo">
                            <h5 class="card-title text-center">{{__('ui.telefono')}} : {{Auth::user()->userprofile->telephone}}</h5>
                            <h5 class="card-title text-center">{{__('ui.indirizzo')}} : {{Auth::user()->userprofile->address}}</h5>
                            <h5 class="card-title text-center">{{__('ui.età')}} : {{Auth::user()->userprofile->age}}</h5>
                            
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                {{__('ui.eliminaAnn')}}
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="exampleModalLabel">{{__('ui.eliminaProfilo')}}</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        {{__('ui.eliminaAnn')}}
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{__('ui.annulla')}}</button>
                                    <form action="{{route('elimina')}}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger" type="submit">{{__('ui.eliminaAnn')}}</button>
                                    </form>
                                    </div>
                                </div>
                                </div>
                            </div>



                            {{-- <form action="{{route('elimina')}}" method="POST">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger" type="submit">Elimina</button>
                            </form> --}}

                        </div>
                    </div>
            </div>
        </div>
    
    @else
    <livewire:userprofile-form>
    @endif

    <h2 class="mt-5 text-center">{{__('ui.favoriti')}}</h2>
    <div class="container">
        <div class="row justify-content-center">
            @foreach($favorite as $announcement)
            <div class="col-12 col-md-3 my-2 py-5 d-flex justify-content-center">
                <div class="card" style="width: 18rem;">
                    <img src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400,300) : "https://picsum.photos/200/300"}}" class="card-img-top" alt="immagine annuncio">
                  <div class="card-body">
                    <h5 class="card-title">{{$announcement->title}}</h5>
                    <div class="d-flex justify-content-between">
                        <a href="{{route('indexDetail', compact('announcement'))}}" class="btn btn-primary">{{__('ui.vaiAiDettagli')}}</a>
                        {{-- implementare toggle cari frontendisti --}}
                                
                        <form action="{{route('favorites', compact ('announcement'))}}" method="POST">
                          @csrf
                          <button type="submit" class="likeButton">
                            <i class="fa-solid fa-heart text-danger"></i>
                        </button>
                        </form>
                      </div>
                  </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>


</x-layout>