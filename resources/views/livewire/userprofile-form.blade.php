<div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center"> {{__('ui.createProf')}} </h1>
                <form wire:submit.prevent="createUserprofile" enctype="multipart/form-data">
                   
                    <div class="mb-3">
                      <label class="form-label">{{__('ui.telefono')}}</label>
                      <input wire:model.debounce.2000ms='telephone' type="number" class="form-control">
                      @error('telephone') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="mb-3">
                        <label class="form-label">{{__('ui.indirizzo')}}</label>
                        <input wire:model.debounce.2000ms='address' type="text" class="form-control">
                        @error('address') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="mb-3">
                        <label class="form-label">{{__('ui.età')}}</label>
                        <input wire:model.debounce.2000ms='age' type="number" class="form-control">
                        @error('age') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="mb-3">
                        <label class="form-label">{{__('ui.foto')}}</label>
                        <input wire:model='photo' type="file" class="form-control">
                        @error('photo') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">{{__('ui.crealo')}}</button>
                  </form>
            </div>
        </div>
    </div>
</div>
