<div>
  {{-- <div class="container w-75">

    <form wire:submit.prevent="createAnnouncement" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
          <label class="form-label">{{__('ui.titolo')}}</label>
          <input wire:model.debounce.2000ms='title' type="text" class="form-control">
          @error('title') <span class="error text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="mb-3">
          <label for="category" class="form-label">{{__('ui.categoria')}}</label>
          <select class="form-select" wire:model.defer="category" id="category">
            <option value="">{{__('ui.selezionaCategoria')}}</option>
            @foreach ($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="mb-3">
            <label class="form-label">{{__('ui.prezzo')}}</label>
            <input wire:model.debounce.2000ms='price' type="number" class="form-control">
            @error('price') <span class="error text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="mb-3">
            <label class="form-label">{{__('ui.descrizione')}}</label>
            <input wire:model.debounce.2000ms='description' type="text" class="form-control">
            @error('description') <span class="error text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="mb-3">
          <label class="form-label">{{__('ui.inserisciImmagine')}}</label>
          <input type="file" wire:model='temporary_images' name='images' multiple class="form-control shadow @error('temporary_images.*') is-invalid @enderror" placeholder="Img/">
          @error('temporary_images') <span class="error text-danger">{{ $message }}</span> @enderror
        </div>
        @if (!empty($images))
        <div class="row">
          <div class="col-12">
            <p>Anteprima immagini</p>
            <div class="row border-4 border-info rounded shadow py-4">
              @foreach ($images as $key=>$image)
                  <div class="col my-3 ">
                    <div class="">
                      <div class="img-preview mx-auto shadow rounded" style="background-image: url({{$image->temporaryUrl()}});"></div>
                        <button class="btn btn-danger shadow d-block text-center mt-2 mx-auto" type="button" wire:click='removeImage({{$key}})'>{{__('ui.annulla')}}</button>
                    </div>
                    </div>
              @endforeach
            </div>
          </div>
        </div>
        @endif
        <button class="btn btn-primary">{{__('ui.crealo')}}</button>
      </form>
  </div> --}}

  <div class="container contact-form">
    <div class="contact-image">
        <img src="https://image.ibb.co/kUagtU/rocket_contact.png" alt="rocket_contact"/>
    </div>
    <form wire:submit.prevent="createAnnouncement" enctype="multipart/form-data">
      @csrf
      <h1 class="my-3">{{__('ui.creaAnnuncio')}}</h1>
       <div class="row">
            <div class="col-md-6 ">
                <div class="form-group pb-2">
                    <input type="text" wire:model.debounce.2000ms='title' class="form-control" placeholder="{{__('ui.titolo')}}" value="" />
                    @error('title') <span class="error text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="form-group py-2">
                    <input type="number" wire:model.debounce.2000ms='price' class="form-control" placeholder="{{__('ui.prezzo')}}" value="" />
                    @error('price') <span class="error text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="form-group py-2">
                  <select class="form-select form-dropdown" wire:model.defer="category" id="category">
                    <option value="">{{__('ui.selezionaCategoria')}}</option>
                    @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                    <textarea wire:model.debounce.2000ms='description' class="form-control" placeholder="{{__('ui.descrizione')}}" style="width: 100%; height: 150px;"></textarea>
                    @error('description') <span class="error text-danger">{{ $message }}</span> @enderror
                </div>
              </div>
              
                <div class="form-group py-1">
                  <input type="file" wire:model='temporary_images' name='images' multiple class="form-control shadow @error('temporary_images.*') is-invalid @enderror" placeholder="Img/">
                  @error('temporary_images') <span class="error text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="form-group py-1">
                  <button type="submit" name="btnSubmit" class="btnContact my-3">{{__('ui.crealo')}}</button>
              </div>
                <div class="form-group py-1">
                  @if (!empty($images))
                  <div class="row">
                    <div class="col-12">
                      <p>Anteprima immagini</p>
                      <div class="row justify-content-evenly border-4 border-info rounded shadow py-4">
                        @foreach ($images as $key=>$image)
                            <div class="col-12 col-md-3 my-3 ">
                              <div class="">
                                <div class="img-preview mx-auto shadow rounded" style="background-image: url({{$image->temporaryUrl()}});"></div>
                                  <button class="btn btn-danger shadow d-block text-center mt-2 mx-auto" type="button" wire:click='removeImage({{$key}})'>{{__('ui.annulla')}}</button>
                              </div>
                              </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                  @endif
                </div>
                
            </div>
            

        </div>
    </form>
</div>

</div>
