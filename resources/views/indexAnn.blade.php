<x-layout>
    <div class="container">
        <div class="row"> 
                <h1 class="my-5 text-center ">{{__('ui.allAnnouncements')}}</h1>
                <livewire:index-ann></livewire:index-ann>
        </div>
    </div>
</x-layout>