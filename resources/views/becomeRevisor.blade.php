<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Presto.it</title>
</head>
<body>
    <div>
        <h1>{{__('ui.userMessage')}}</h1>
        <p>{{__('ui.nome')}} : {{$user->name}}</p>
        <p>{{__('ui.email')}} : {{$user->email}}</p>
        <h4>{{__('ui.makeRevisor')}}</h4><a href="{{route('makeRevisor',compact('user'))}}"> {{__('ui.click')}}</a>
    </div>
    
</body>
</html>