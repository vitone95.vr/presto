<x-layout>
  <div class="container-fluid containerDetail vh-100">
     <div class="container containerDetail">
       
       <div class="row justify-content-evenly py-5">
  
           <div class="col-12 col-md-6">
              <section class="borderDettAnn">
                  <div class="container">
                    @if ($announcement->images->count() > 0)
                      <div class="carousel">
                          @foreach($announcement->images as $image)
                            <input type="radio" name="slides" @if($loop->first)checked="checked" @endif id="slide-{{$loop->iteration}}">
                          @endforeach
                          <ul class="carousel__slides">
                            @foreach($announcement->images as $image)
                                <li class="carousel__slide">
                                  <figure>
                                    <div>
                                      <img src="{{Storage::url($image->path)}}" class="img-fluid rounded " alt="immagine annuncio">
                                    </div>
                                  </figure>
                                </li>
                            @endforeach
                            </ul> 
                            <ul class="carousel__thumbnails">
                            @foreach($announcement->images as $image)
                                <li>
                                    <label for="slide-{{$loop->iteration}}"><img src="{{Storage::url($image->path)}}" class="img-fluid rounded " alt="immagine annuncio"></label>
                                </li>
                            @endforeach 
                          </ul>
                      </div>
                    @else
                      <div class="carousel">
                         <input type="radio" name="slides" checked="checked" id="slide-1">
                         <input type="radio" name="slides" id="slide-2">
                         <input type="radio" name="slides" id="slide-3">
                         <input type="radio" name="slides" id="slide-4">
                         <ul class="carousel__slides">
                              <li class="carousel__slide">
                                <figure>
                                  <div>
                                    <img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio">
                                  </div>
                                </figure>
                              </li>
                              <li class="carousel__slide">
                                <figure>
                                  <div>
                                    <img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio">
                                  </div>
                                </figure>
                              </li>
                              <li class="carousel__slide">
                                <figure>
                                  <div>
                                    <img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio">
                                  </div>
                                </figure>
                              </li>
                              <li class="carousel__slide">
                                <figure>
                                  <div>
                                    <img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio">
                                  </div>
                                </figure>
                              </li>
                          </ul>
                          <ul class="carousel__thumbnails">
                                <li>
                                    <label for="slide-1"><img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio"></label>
                                </li>
                                 <li>
                                  <label for="slide-2"><img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio"></label>
                                </li>
                                <li>
                                  <label for="slide-3"><img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio"></label>
                                </li>
                                <li>
                                   <label for="slide-4"><img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio"></label>
                                </li>
                          </ul>
                      </div>
                    @endif
                  </div>
              </section>

            </div>          
            <div class="col-12 col-md-6  d-flex flex-column justify-content-evenly align-items-center ">
                 
                    <h2 class="fw-bold">{{$announcement->title}}</h5>
                    <h5 >{{__('ui.categoria')}}: {{$announcement->category->name}}</h5>
                    <h5 >€ {{$announcement->price}}</h5>
                    <h5 >{{__('ui.descrizione')}}: {{$announcement->description}}</h5>
                    <p class="fst-italic" >{{__('ui.autore')}} :  @if ($announcement->user != null) {{$announcement->user->name}} @else {{__('ui.autoreAssente')}} @endif</p>
                    <p class="fst-italic">{{__('ui.pubblicato')}} : {{$announcement->created_at}}</p>

                    @Auth
                    <div class="row">
                        {{-- implementare toggle cari frontendisti --}}
                        <form action="{{route('favorites', compact ('announcement'))}}" method="POST">
                      @csrf
                      <button type="submit" class="likeButton">
                        
                      </button>
                       </form>

                        @if(Auth::user()->id == $announcement->user_id)
                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal">
                      {{__('ui.eliminaAnn')}}
                    </button>
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel">{{__('ui.eliminaAnn')}}</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                          </div>
                          <div class="modal-body">
                            <p>Sei sicuro di voler eliminare il tuo annuncio?</p>
                            <p>L'operazione è irreversibile</p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{__('ui.annulla')}}</button>
                            <form action="{{route('indexDelete', compact('announcement'))}}" method="POST">
                               @method('delete')
                               @csrf
                               <button class="btn btn-danger" type="submit">{{__('ui.eliminaAnn')}}</button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endif
                  @endauth
            </div>
            
        </div>
    </div>
</div>






</x-layout>
