{{-- INIZIO NAV PRINCIPALE --}}
<nav class="navbar navbar-expand-lg px-4 navbarSupportedContent">
    <div class="container-fluid">
            {{-- logo --}}
            <a href="{{route('/')}}"><img class="logotemporaneo" src="/img/logoFinale.png" alt="logo-temporaneo"></a>


        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse mx-5" id="navbarSupportedContent">
            <ul class="navbar-nav   mb-2  mb-lg-0">

                <li class="nav-item">
                    <a class="nav-link" href="{{route('indexAnn')}}">{{__('ui.annunci')}}</a>
                </li>
            </ul>

                @guest
                <div class="ms-auto">

                    <a class="mx-2 text-decoration-none" href="{{route('login')}}"><button class="bn632-hover bn26">
                        {{__('ui.accedi')}}
                    </button></a>

                    <a class="mx-2 text-decoration-none"  href="{{route('register')}}"><button class="bn632-hover bn26">
                        {{__('ui.registrati')}}
                      </button></a>

               </div>

               @else
               @if (Auth::user()->is_revisor)
               {{-- <div class="container"> --}}
               <ul class="navbar-nav  mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link position-relative" href="{{route('index')}}">{{__('ui.zonaRevisori')}}
                    <span class="bg-danger position-absolute translate-middle badge rounded-pill start-80 top-0">
                        {{App\Models\Announcement::toBeRevisionedCount()}}
                        <span class="visually-hidden">Messaggi non letti</span>
                    </span>
                    </a>
                </li>
                @endif
                <ul class="navbar-nav ">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('creaAnn')}}">{{__('ui.inserisciAnnuncio')}}</a>
                </li>
                    <li class="nav-item dropdown link-username">
                      <a class="nav-link dropdown-toggle fw-bold" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        {{__('ui.benvenuto')}} {{ Auth::user()->name }}
                      </a>
                      <ul class="dropdown-menu dropdown-menu dropdown-utente" style="z-index: 2000">
                        <li class="py-1"><a class="dropdown-item" href="{{route('userpage')}}">{{__('ui.profilo')}}</a></li>

                        <li class="py-1"><a class="dropdown-item" href="{{route('userAnn')}}">{{__('ui.tuoiAnn')}}</a></li>

                        <li class="py-1"><a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{__('ui.logout')}}</a></li>
                        <form id="logout-form" action="{{route('logout')}}" method="POST" class="d-none">

                      </ul>
                    </li>
                  </ul>
                    @csrf
                </form>
                @endguest
            </ul>
        {{-- </div> --}}


        </div>
    </div>
</nav>
{{-- FINE NAVBAR PRINCIPALE --}}


{{-- INIZIO NAV SECONDARIA --}}
<nav class="navbar sticky-top bg-dark">
    <div class="container-fluid justify-content-around mt-1">
      <form action="{{route('searchAnn')}}" method="GET" class="d-flex" >
        <input class="form-control me-2" type="search" name="searched" placeholder="{{__('ui.cercaAnnuncio')}}" >
        <button class="btn btn-primary" type="submit">{{__('ui.cerca')}}</button>
      </form>
      <div class="dropdown my-3">
        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            {{__('ui.filtraCategoria')}}
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            {{-- Tanto roberto ha detto di star tranquillo --}}
          @foreach ($categories as $category)
          <li><a class="dropdown-item" href="{{route('categoryShow', compact ('category'))}}">{{$category->name}}</a></li>
          @endforeach
        </ul>
      </div>
    </div>
  </nav>
{{-- FINE NAV SECONDARIA --}}
