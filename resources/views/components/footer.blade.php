{{-- <div class="container-fluid">
  <footer class="row colorgradientfoot py-5 my-5 p-2 mb-2 ">
    <div class="col-6 mb-3">
      <h5>Lavora con noi</h5>
      <a href="{{route('becameRevisor')}}" class="btn btn-warning">Diventa revisore</a>
      <ul class="nav flex-column">
      <p class="">Presto © 2022</p>
    </div>
    <div class="col-6 mb-3 d-flex justify-content-center">
        <h5>Links</h5>
        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">Home</a></li>
        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">Catalogo annunci</a></li>
        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">Pricing</a></li>
        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">FAQs</a></li>
        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">About</a></li>
      </ul>
    </div>
  </footer>
</div> --}}


        <footer class="bg-dark text-center text-white">
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-12 col-md-4 mt-4 my-5">
                    <p>
                        <strong>{{__('ui.lavoraConNoi')}}</strong>
                    </p>
                    <a href="{{route('becameRevisor')}}" class="btn btn-warning"> {{__('ui.diventaRevisore')}}</a>
                </p>
                </div>
                <div class="col-12 col-md-4 mt-4 my-5">
                <h5 class="text-uppercase">{{__('ui.linkUtili')}}</h5>
                    <ul class="list-unstyled ">
                        <li>
                            <a href="{{route('/')}}" class="text-white">{{__('ui.home')}}</a>
                        </li>
                        <li>
                            <a href="{{route('indexAnn')}}" class="text-white">{{__('ui.annunci')}}</a>
                        </li>
                        <li>
                            <a href="#!" class="text-white">{{__('ui.faq')}}</a>
                        </li>
                        <li>
                            <a href="#!" class="text-white">{{__('ui.about')}}</a>
                        </li>
                        <li class="d-flex justify-content-evenly mt-5">
                            <x-_locale lang="en" nation="gb"/>
                            <x-_locale lang="it" nation="it"/>
                            <x-_locale lang="ru" nation="ru"/>
                        </li>
                    </ul>
            </div>
            <div class="col-12 col-md-4 mt-4 mb-4 ">
                    <p class="mt-4"> <strong>
                        {{__('ui.slogan')}}</strong>
                    
                </div>
            </div>
            <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
                Presto © 2022</div>
        </footer>

