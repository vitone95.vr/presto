<x-layout>
        {{-- SECONDO LOGIN-PROVA --}}
            <div class="container-totale">
                    <div class="box">
                    <form class="form"action="{{route('login')}}" method="POST">
                    @csrf
                    <h2>{{__('ui.accedi')}}</h2>
                    {{-- <p>Inserisci la tua email e la password!</p> --}}
                    <div class="inputBox">
                        <input class="loginput" type="email" required="required" name="email">
                        <span>{{__('ui.insertDati')}}</span>
                        <i></i>
                    </div>
                    <div class="inputBox">
                        <input class="loginput" type="password" required="required" name="password">
                        <span>{{__('ui.password')}}</span>
                        <i></i>
                    </div>
                    <div class="links">
                    <a href="/">{{__('ui.passDimenticata')}}</a>
                    <a href="{{route('register')}}">{{__('ui.registrati')}}</a>
                    </div>
                    <button class="btn-accedi" type="submit">{{__('ui.accedi')}}</button>
                </form>
            </div>
        {{-- SECONDO LOGIN-PROVA FINE --}}
</x-layout>
