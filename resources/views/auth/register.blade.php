<x-layout>
{{-- REGISTER definitivo --}}
<div class="container-totale">
        <div class="boxRegister">
        <form class="formRegistrati" action="{{route('register')}}" method="POST">
        @csrf
        <h2>{{__('ui.registrati')}}</h2>
        {{-- <p>Inserisci la tua email e la password!</p> --}}
        <div class="inputBox">
            <input class="loginput" type="name" required="required" name="name">
            <span>{{__('ui.nomeUtente')}}</span>
            <i></i>
        </div>
        <div class="inputBox">
            <input class="loginput" type="email" required="required" name="email">
            <span>{{__('ui.email')}}</span>
            <i></i>
        </div>
        <div class="inputBox">
            <input class="loginput" type="password" required="required" name="password">
            <span>{{__('ui.password')}}</span>
            <i></i>
        </div>
        <div class="inputBox">
            <input class="loginput" type="password" required="required" name="password_confirmation">
            <span>{{__('ui.passwordConfirm')}}</span>
            <i></i>
        </div>
        <button class="btn-accedi" type="submit">{{__('ui.registrati')}}</button>
    </form>
</div>
{{-- fine register definitivo --}}


</x-layout>
