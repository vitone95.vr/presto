<x-layout>
    @if (session('access.denied'))
    <div class="alert alert-danger">
        {{ session('access.denied') }}
    </div>
    @endif
    @if (session('become'))
    <div class="alert alert-success">
        {{ session('become') }}
    </div>
    @endif
    @if (session('make'))
    <div class="alert alert-success">
        {{ session('make') }}
    </div>
    @endif
    {{-- <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="my-5">{{__('ui.homeTitle')}}</h1>
            </div> --}}
        {{-- typewriter + 3D --}}
        {{-- Container Wrapper --}}
            <div id="container-type-3D">
                <div class="row">
                {{-- 3D holder --}}
                    <div class="col-12 col-6">
                        <div id="aSide">
                            <model-viewer src="/img/earthpoly.glb" alt="earth" auto-rotate  ar ios-src="assets/pianeta/">
                            </model-viewer>
                        </div>
                    </div>
                {{-- FINE 3D holder --}}
                {{-- typewriter inizio --}}
                    <div class="col-12 col-6">
                        <div class="container-typewriter">
                        <p class="typewriter">
                            <span data-text="BUY FAST, SELL FASTER"></span>
                        </p>
                        </div>
                    </div>
                {{-- FINE typewriter inizio --}}
                {{-- coso vai agli annunci --}}
                <h2 class="mx-5 vedi-annunci">{{__('ui.vuoiVedereAnnunci')}} <a href="{{route('indexAnn')}}"><button class="bn632-hover btn-ann">{{__('ui.vaiAnnunci')}}</button></a> </h2>
                {{-- coso vai agli annunci --}}

                </div>
                <div class='scrolldown'>
                    <div class="chevrons">
                        <div class='chevrondown'></div>
                        <div class='chevrondown'></div>
                    </div>
                    </div>
            </div>
        {{-- FINE typewriter + 3D --}}


            @auth
            <div class="col-12 text-center my-5">
                <h2>{{__('ui.vuoiCreareAnnuncio')}}</h2>
                   <a class="btn btn-primary" href="{{route('creaAnn')}}">{{__('ui.crealo')}}</a>
            </div>
            @endauth
            <h2 class="text-center my-5">{{__('ui.annunciCliccati')}}</h2>
            <div class="row">
                    @foreach($announcements as $announcement)
                        <div class="col-12 col-md-3 my-2 py-3 ">
                            <div class="card-profile" >
                                <img src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400,300) : "https://picsum.photos/400/300"}}"  class="img img-responsive" alt="immagine annuncio">

                                <div class="card-content">
                                    <h5 class="card-title my-2">{{$announcement->title}}</h5>
                                    <div class="d-flex justify-content-between">
                                        <a href="{{route('indexDetail', compact('announcement'))}}" class="btn btn-primary rounded text-center">{{__('ui.vaiAiDettagli')}}</a>
                                        {{-- implementare toggle cari frontendisti --}}
                                        @auth
                                        <form action="{{route('favorites', compact ('announcement'))}}" method="POST">
                                            @csrf
                                            <button type="submit" class="likeButton">
                                              <i class="fa-solid fa-heart text-danger"></i>
                                          </button>
                                          </form>
                                        @endauth
                                      </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
            </div>


        </div>
    </div>

</x-layout>
