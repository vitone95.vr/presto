<x-layout>
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
        <form action="{{ route('undo_announcement', compact('lastReview')) }}"
                method="POST">
                @csrf
                @method('PATCH')
                <button type="submit" class="btn btn-danger shadow">{{__('ui.annulla')}}</button>
            </form>
    </div>
    @endif
    @if (session('denied'))
    <div class="alert alert-danger">
        {{ session('denied') }}
        <form action="{{ route('undo_announcement', compact('lastReview')) }}"
                method="POST">
                @csrf
                @method('PATCH')
                <button type="submit" class="btn btn-danger shadow">{{__('ui.annulla')}}</button>
            </form>
    </div>
    @endif
    @if (session('undo'))
    <div class="alert alert-warning">
        {{ session('undo') }}
    </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center my-5">
                    @if($announcement_to_check) {{__('ui.annToRevise')}} @else {{__('ui.noAnnToRevise')}} @endif
                </h1>
            </div>
        </div>
    </div>
 
    
    @if($announcement_to_check)
    <div class="container">
        <div class="row justify-content-evenly">
                <div class="col-12 col-md-6">
                    <section class="borderDettAnn">
                        <div class="container">
                          @if ($announcement_to_check->images->count() > 0)
                            <div class="carousel">
                                @foreach($announcement_to_check->images as $image)
                                  <input type="radio" name="slides" @if($loop->first)checked="checked" @endif id="slide-{{$loop->iteration}}">
                                @endforeach
                                <ul class="carousel__slides">
                                  @foreach($announcement_to_check->images as $image)
                                      <li class="carousel__slide">
                                        <figure>
                                          <div>
                                            <img src="{{Storage::url($image->path)}}" class="img-fluid rounded " alt="immagine annuncio">
                                          </div>
                                        </figure>
                                      </li>
                                  @endforeach
                                  </ul> 
                                  <ul class="carousel__thumbnails">
                                  @foreach($announcement_to_check->images as $image)
                                      <li>
                                          <label for="slide-{{$loop->iteration}}"><img src="{{Storage::url($image->path)}}" class="img-fluid rounded " alt="immagine annuncio"></label>
                                      </li>
                                  @endforeach 
                                </ul>
                            </div>
                          @else
                            <div class="carousel">
                               <input type="radio" name="slides" checked="checked" id="slide-1">
                               <input type="radio" name="slides" id="slide-2">
                               <input type="radio" name="slides" id="slide-3">
                               <input type="radio" name="slides" id="slide-4">
                               <ul class="carousel__slides">
                                    <li class="carousel__slide">
                                      <figure>
                                        <div>
                                          <img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio">
                                        </div>
                                      </figure>
                                    </li>
                                    <li class="carousel__slide">
                                      <figure>
                                        <div>
                                          <img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio">
                                        </div>
                                      </figure>
                                    </li>
                                    <li class="carousel__slide">
                                      <figure>
                                        <div>
                                          <img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio">
                                        </div>
                                      </figure>
                                    </li>
                                    <li class="carousel__slide">
                                      <figure>
                                        <div>
                                          <img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio">
                                        </div>
                                      </figure>
                                    </li>
                                </ul>
                                <ul class="carousel__thumbnails">
                                      <li>
                                          <label for="slide-1"><img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio"></label>
                                      </li>
                                       <li>
                                        <label for="slide-2"><img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio"></label>
                                      </li>
                                      <li>
                                        <label for="slide-3"><img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio"></label>
                                      </li>
                                      <li>
                                         <label for="slide-4"><img src="https://picsum.photos/200/300" class="img-fluid rounded " alt="immagine annuncio"></label>
                                      </li>
                                </ul>
                            </div>
                          @endif
                        </div>
                    </section>
                </div>
                        <div class="col-12 col-md-6  d-flex flex-column justify-content-evenly align-items-center ">
                 
                            <h2 class="fw-bold mt-4">{{$announcement_to_check->title}}</h5>
                            <h5 >{{__('ui.categoria')}}: {{$announcement_to_check->category->name}}</h5>
                            <h5 >€ {{$announcement_to_check->price}}</h5>
                            <h5 >{{__('ui.descrizione')}}: {{$announcement_to_check->description}}</h5>
                            <p class="fst-italic" >{{__('ui.autore')}} :  @if ($announcement_to_check->user != null) {{$announcement_to_check->user->name}} @else {{__('ui.autoreAssente')}} @endif</p>
                            <p class="fst-italic">{{__('ui.pubblicato')}} : {{$announcement_to_check->created_at}}</p>
        
                            <div class="row">
                                    <div class="col-6">
                                        <form action="{{route('accept_announcement',['announcement'=>$announcement_to_check])}}" method="POST">
                                        @csrf
                                        @method('PATCH')
                                            <button type="submit" class="btn btn-success shadow my-3">{{__('ui.accetta')}}</button>
                                        </form>
                                    </div>
                                    <div class="col-6 text-end">
                                    <form action="{{route('reject_announcement',['announcement'=>$announcement_to_check])}}" method="POST">
                                        @csrf
                                        @method('PATCH')
                                            <button type="submit" class="btn btn-danger shadow my-3 px-3">{{__('ui.rifiuta')}}</button>
                                        </form>
                                    </div>
                              </div>
                       </div>
                

        </div>
        <div class="row py-4">
                <div>
                    <h5 class="tc-accent">Tags</h5>
                    @if ($image->labels)
                        @foreach ($image->labels as $label)
                            <p class="d-inline">{{$label}},</p>
                        @endforeach
                    @endif
                </div>
                <div class="py-4">
                    <h5 class="tc-accent">Etichette Immagini</h5>
                    <p class="d-inline px-2">Adulti: <span class="{{$image->adult}}"></span></p>
                    <p class="d-inline px-2">Satira: <span class="{{$image->spoof}}"></span></p>
                    <p class="d-inline px-2">Medicina: <span class="{{$image->medical}}"></span></p>
                    <p class="d-inline px-2">Violenza: <span class="{{$image->violence}}"></span></p>
                    <p class="d-inline px-2">Contenuto ammiccante: <span class="{{$image->racy}}"></span></p>
                </div>
        </div>
    </div>
    @endif
</x-layout>
