<x-layout>
    @if (session('create'))
    <div class="alert alert-success">
        {{ session('create') }}
    </div>
    @endif
    <div class="cointainer">
        <div class="row">
            <div class="col-12 text-center">
            @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
            @endif
            
            <livewire:create-announcement>
        </div>
    </div>
</div>
</x-layout>