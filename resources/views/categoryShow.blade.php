<x-layout>

    <div class="container">
        <h1 class="text-center my-5">
            {{$category->name}}
        </h1>
        <div class="row justify-content-evenly">
            @forelse($announcements as $announcement)
            <div class="col-12 col-md-4 my-2 py-3  " 
            data-aos="zoom-in"
            data-aos-duration="250"
            data-aos-offset="0">
              <div class="card-profile" style="width: 18rem;">
                <img src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400,300) : "https://picsum.photos/400/300"}}" class="img img-responsive" alt="immagine annuncio">

                <div class="card-content">
                  <h5 class="card-title">{{$announcement->title}}</h5>
                  <div class="d-flex justify-content-between">
                    <a href="{{route('indexDetail', compact('announcement'))}}" class="btn btn-primary">{{__('ui.vaiAiDettagli')}}</a>
                    {{-- implementare toggle cari frontendisti --}}
                    @auth        
                    <form action="{{route('favorites', compact ('announcement'))}}" method="POST">
                      @csrf
                      <button type="submit" class="likeButton">
                        @if(Auth::user()->whereHasFavorites(Auth::user()->id))
                        <i class="fa-solid fa-heart text-danger"></i>
                        @else
                        <i class="fa-regular fa-heart"></i>
                        @endif
                    </button>
                    </form>
                    @endauth
                  </div>
                </div>
              </div>
            </div>
            @empty         
            <div class="col-12">
              <h2>{{__('ui.categoryNoAnnouncements')}}</h2>
              <h3>{{__('ui.publicAnnouncement')}} <a href="{{route('creaAnn')}}">{{__('ui.nuovoAnnouncio')}}</a></h3>
            </div>
            @endforelse
        </div>
    </div>
</x-layout>