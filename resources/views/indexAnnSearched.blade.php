<x-layout>
    <h1 class="text-center mt-2">
        {{__('ui.searchResult')}}
    </h1>
    <div class="container">
        <div class="row justify-content-center">
            @forelse($announcements as $announcement)
               <div class="col-12 col-md-3 my-3">
                <div class="card-profile">
                  <img src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400,300) : "https://picsum.photos/200/300"}}" class="img img-responsive" alt="immagine annuncio">

                  <div class="card-content">
                    <h5 class="card-title my-2">{{$announcement->title}}</h5>
                    <div class="d-flex justify-content-between">
                      <a href="{{route('indexDetail', compact('announcement'))}}" class="btn btn-primary">{{__('ui.vaiAiDettagli')}}</a>
                      {{-- implementare toggle cari frontendisti --}}
                      @auth   
                      <form action="{{route('favorites', compact ('announcement'))}}" method="POST">
                        @csrf
                        <button type="submit" class="likeButton">
                          <i class="fa-solid fa-heart text-danger"></i>
                      </button>
                      </form>
                      @endauth
                    </div>
                  </div>
                </div>
            </div>
            @empty
               <div class="col-12">
                <div class="alert alert-warning">
                  <h3>
                    {{__('ui.searchFailed')}}
                  </h3>
                </div>
               </div> 
            @endforelse
        </div>
        {{$announcements->links()}}
    </div>
</x-layout>