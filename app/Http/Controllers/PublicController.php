<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\UserProfile;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Maize\Markable\Markable;
use Maize\Markable\Models\Favorite;
use Illuminate\Support\Facades\Auth;


class PublicController extends Controller
{
    public function home(){
        $announcements = Announcement::where('is_accepted', true)->popularAllTime()->take(4)->get();
        return view('welcome', compact('announcements'));
    }

    public function creaAnn(){
        return view('creaAnn');
    }

    public function indexAnn(){
        return view ('indexAnn');
    }

    public function categoryShow(Category $category){
        $announcements = $category->announcements()->where('is_accepted',true)->get();
        return view('categoryShow', compact('category'),compact('announcements'));
    }
    
    public function indexDetail(Announcement $announcement,){
        $announcement->visit();
        return view('indexDetail', compact('announcement'));
    }

    public function searchAnn(Request $request){
        $announcements = Announcement::search($request->searched)->where('is_accepted', true)->paginate(6);
        return view('indexAnnSearched', compact('announcements'));
    }

    public function setLanguage($lang){
        session()->put('locale',$lang);
        return redirect()->back();
    }

    public function userpage(){
        $profile = Auth::user()->userprofile()->get();
        $favorite = Announcement::whereHasFavorite(auth()->user())->get();
        return view('userPage',compact('profile', 'favorite'));
    }

    public function favorites(Announcement $announcement){
        $announcement = Announcement::findOrFail($announcement->id);
        $user = auth()->user();
        Favorite::toggle($announcement, $user);
        return redirect()->back();
    }
    
    public function indexDelete(Announcement $announcement){
        $announcement->delete();
        return view ('creaAnn');
    }

    public function elimina(){
        Auth::user()->userprofile()->delete();
        return redirect(route('userpage'))->with('delete','Hai eliminato con successo il tuo profilo');
    }

    public function userAnn(){
        $announcements = Auth::user()->announcements()->where('is_accepted', true)->paginate(6);
        return view('userAnn', compact('announcements'));
    }
   
}