<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Mail\BecomeRevisor;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class RevisorController extends Controller
{
    public function index(){
        $announcement_to_check= Announcement::where('is_accepted', null)->first();
        $lastReview = Announcement::all()->sortByDesc('updated_at')->first();
        return view('index', compact('announcement_to_check'),compact('lastReview'));
    }

    public function acceptAnnouncement(Announcement $announcement){
        $announcement->setAccepted(true);
        return redirect(route('index'))->with('success',trans('ui.success'));
    }

    public function rejectAnnouncement(Announcement $announcement){
        $announcement->setAccepted(false);
        return redirect(route('index'))->with('denied',trans('ui.failure'));
    }

    public function undoAnnouncement(Announcement $lastReview){
        $lastReview->setAccepted(null);
        return redirect(route('index'))->with('undo',trans('ui.undo'));
    }

    public function becomeRevisor(){
        Mail::to('admin@admin.it')->send(new BecomeRevisor(Auth::user()));
        return redirect(route('/'))->with('become',trans('ui.become'));
    }

    public function makeRevisor(User $user){
        Artisan::call('presto:makeUserRevisor',['email'=>$user->email]);
        return redirect('/')->with('make',trans('ui.make'));
    }
}
