<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use App\Jobs\RemoveFaces;
use App\Jobs\ResizeImage;
use App\Models\Announcement;
use App\Jobs\InsertWatermark;
use Livewire\WithFileUploads;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class CreateAnnouncement extends Component
{
    use WithFileUploads;
    
    public $title;
    public $price;
    public $description;
    public $category;
    public $temporary_images;
    public $images= [];
    public $image;
    public $announcement;

    protected $rules = [
        'title'=>'required|min:4',
        'category'=>'required',
        'description'=>'required|min:8',
        'price'=>'required|numeric',
        'temporary_images.*'=>'image|max:1024',
        'images.*'=>'image|max:1024',
    ];

    protected $messages = [
        'required'=> 'il campo :attribute è richiesto',
        'min'=> 'il campo :attribute è troppo corto',
        'numeric'=> 'il campo :attribute deve essere un numero',
        'image'=> 'il campo :attribute deve essere una immagine',
    ];

    public function updatedTemporaryImages()
    {
        if($this->validate(['temporary_images.*'=>'image|max:1024'])){
            foreach($this->temporary_images as $image){
                $this->images[]= $image;
            }
        }
    }

    public function removeImage($key){
        if (in_array($key,array_keys($this->images))) {
            unset($this->images[$key]);
        }
    }

    public function createAnnouncement(){
    
        $this->validate();
        $this->announcement = Category::find($this->category)->announcements()->create($this->validate());
        if (count($this->images)) {
            foreach ($this->images as $image) {
                /* $this->announcement->images()->create(['path'=>$image->store('image','public')]); */
                $newFileName = "announcements/{$this->announcement->id}";
                $newImage = $this->announcement->images()->create(['path'=>$image->store($newFileName,'public')]);

                RemoveFaces::withChain([
                    new ResizeImage($newImage->path, 400, 300),
                    new GoogleVisionSafeSearch($newImage->id),
                    new GoogleVisionLabelImage($newImage->id),
                    new InsertWatermark($newImage->id)
                ])->dispatch($newImage->id);

            }
            File::deleteDirectory(storage_path('/app/livewire-tmp'));
        }
        $this->announcement->user()->associate(Auth::user());
        $this->announcement->save();

        return redirect(route('creaAnn'))->with('create','Annuncio inserito correttamente,verrà revisionato a breve!');
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function render()
    {
        return view('livewire.create-announcement');
    }
}
