<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Announcement;

class IndexAnn extends Component
{
    public function render()
    {
        $announcements = Announcement::where('is_accepted', true)->orderBy('created_at','DESC')->paginate(6);
        return view('livewire.index-ann',compact('announcements'));
    }
}
