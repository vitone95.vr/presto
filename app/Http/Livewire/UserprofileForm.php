<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;

class UserprofileForm extends Component
{
    use WithFileUploads;

    public $telephone;
    public $address;
    public $age;
    public $photo;

    protected $rules = [
        'telephone' => 'required',
        'address' => 'required',
        'age' => 'required',
        'photo' => 'required|image',
    ];

    public function updated($propertyName){
        $this->validateOnly($propertyName);
    }

    public function createUserprofile(){
        $this->validate();
        Auth::user()->userprofile()->create([
            'telephone'=>$this->telephone,
            'address'=>$this->address,
            'age'=>$this->age,
            'photo'=>$this->photo->store('public/userphoto'),
        ]);
        return redirect(route('userpage'))->with('message','You have created your user profile correctly!');
    }


    public function render(){
        return view('livewire.userprofile-form');
    }
}
