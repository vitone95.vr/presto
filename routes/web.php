<?php

use App\Models\Category;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RevisorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'home'])->name('/');

Route::get('/crea-annuncio',[PublicController::class, 'creaAnn'])->name('creaAnn')->middleware('auth');

Route::get('/catalogo', [PublicController::class, 'indexAnn'])->name('indexAnn');

Route::get('/categoria/{category}',[PublicController::class, 'categoryShow'])->name('categoryShow');

Route::get('/catalogo/dettaglio/{announcement}',[PublicController::class, 'indexDetail'])->name('indexDetail');

Route::get('/richiesta/revisore',[RevisorController::class, 'becomeRevisor'])->name('becameRevisor')->middleware('auth');

Route::get('/rendi/revisore/{user}',[RevisorController::class, 'makeRevisor'])->name('makeRevisor');

Route::get('/revisore/home', [RevisorController::class,'index'])->name('index')->middleware('isRevisor');

Route::patch('/accetta/annuncio/{announcement}',[RevisorController::class, 'acceptAnnouncement'])->name('accept_announcement');

Route::patch('/rifiuta/annuncio/{announcement}', [RevisorController::class, 'rejectAnnouncement'])->name('reject_announcement');

Route::patch('/annulla/annuncio/{lastReview}', [RevisorController::class, 'undoAnnouncement'])->name('undo_announcement');

Route::get('/cerca/annuncio',[PublicController::class , 'searchAnn'])->name('searchAnn');

Route::post('/lingua/{lang}',[PublicController::class , 'setLanguage'])->name('set_language_locale');

Route::get('/user-profile', [PublicController::class, 'userpage'])->name('userpage')->middleware('auth');

Route::post('/segui-annuncio/{announcement}',[PublicController::class , 'favorites'])->name('favorites');

Route::delete('/elimina/annuncio/{announcement}',[PublicController::class, 'indexDelete'])->name('indexDelete');

Route::delete('/user-profile/elimina', [PublicController::class, 'elimina' ])->name('elimina');

Route::get('/user-profile/annunci', [PublicController::class, 'userAnn'])->name('userAnn');
